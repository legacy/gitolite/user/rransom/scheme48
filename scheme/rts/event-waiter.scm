
;; Based on the condvars package by Richard Kelsey and Jonathan Rees.

;; Event waiters (block threads if there have been no new events since
;; they were last unblocked).
;;
;; Public functions:
;;   (make-event-waiter [id]) -> event-waiter
;;   (event-waiter? obj) -> boolean
;;   (wait-for-new-events ew last-token) -> some-new-token
;;   (signal-event! ew)
;;
;; Semi-public functions (might be useful in new synchronization abstractions):
;;   (event-waiter-token ew) -> some-current-token
;;   (maybe-commit-and-wait-for-new-events ew last-token) -> boolean
;;   (maybe-commit-and-signal-event! ew) -> boolean
;;
;; Semi-private functions (should only be used within RTS):
;;   (event-waiter-queue ew) -> thread-queue

;; Example usage:
;;
;; (let loop1 ((token #f))
;;   (let loop2 ()
;;     (let ((event (maybe-fetch-event-from-outside-world!)))
;;       (if event
;;           (begin (process-event! event)
;;                  (loop2)))))
;;   (loop1 (wait-for-new-events ew token)))
;;
;; The token exists only to ensure that if an event occurs between the
;; time that the thread last checks for (and handles) an event and the
;; time that it calls wait-for-new-events, the thread will not block
;; without handling that new event.

(define-synchronized-record-type event-waiter :event-waiter
  (really-make-event-waiter queue token debug-id)
  (token) ; provisionally manipulated fields
  event-waiter?
  (queue event-waiter-queue)
  (token event-waiter-token set-event-waiter-token!)
  (debug-id event-waiter-debug-id))

(define-record-discloser :event-waiter
  (lambda (ew)
    (let ((ew-id (event-waiter-debug-id ew)))
      (if ew-id
          (list 'event-waiter ew-id)
          '(event-waiter)))))

(define (make-event-waiter . id-option)
  (really-make-event-waiter (make-queue)
                            #f
                            (if (null? id-option)
                                #f
                                (car id-option))))

;; Try to commit the current proposal.  On failure, return #f.  On
;; success, block the current thread until ew's token is not eq? to
;; last-token, then return #t.  (If ew's token is already not eq? to
;; last-token, do not block the current thread.)
;;
;; This function must be called with a proposal active.
(define (maybe-commit-and-wait-for-new-events ew last-token)
  (if (eq? last-token (event-waiter-token ew))
      (maybe-commit-and-block-on-queue (event-waiter-queue ew))
      (maybe-commit)))

;; Try to commit the current proposal; on success, change ew's token
;; to a newly created cell, awakening any threads blocked on ew.
;;
;; This function must be called with a proposal active.
(define (maybe-commit-and-signal-event! ew)
  (set-event-waiter-token! ew (make-cell #f))
  (maybe-commit-and-make-ready (event-waiter-queue ew)))

;; Block the current thread until ew's token is not eq? to last-token.
;; Return a token held by ew at some point after the thread was
;; awakened.
(define (wait-for-new-events ew last-token)
  (with-new-proposal (lose)
    (or (maybe-commit-and-wait-for-new-events ew last-token)
        (lose)))
  (event-waiter-token ew))

;; Change ew's token to a newly created cell, awakening any threads
;; blocked on ew.
(define (signal-event! ew)
  (with-new-proposal (lose)
    (or (maybe-commit-and-signal-event! ew)
        (lose))))

